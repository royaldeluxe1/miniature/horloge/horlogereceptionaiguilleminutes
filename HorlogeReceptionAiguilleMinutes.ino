#include <SoftwareSerial.h>
#include <Stepper.h>

SoftwareSerial serialReception (2, 3);

const int stepsPerRevolution = 300;  // Nombre de pas pour qu'une aiguille face un tour
Stepper stepper(stepsPerRevolution, 4, 5, 6, 7);

int minute = 0;
int pas = 1;    // nombre de pas

int pinL_DipSwitch = A4;
int pinH_DipSwitch = A5;

int pinHeure = 8;
int pinSens = 9;
int pinCapteur = 10;
int pinBouton = 11;

int statePinHeure = LOW;

int s1, s2, s3; //seuil du des valeurs de map
int t1, t2, t3; //valeur de la temporisation
int c;

unsigned long previousMillis = 0;        // sauvegarde last time
unsigned long currentMillis;             //temps actuel

String command;
String part1;
String part2;

int indexSecondeFolle;
int nbPasA [] = { 114, -34, 170, -28, 200, -150, 100, -80, 300, -130};

int stepspeed = 200;

void setup() {

  Serial.begin(9600);
  serialReception.begin(9600);

  stepper.setSpeed(stepspeed);

  pinMode(pinL_DipSwitch, INPUT_PULLUP);
  pinMode(pinH_DipSwitch, INPUT_PULLUP);

  pinMode(pinHeure, OUTPUT);
  pinMode(pinSens, OUTPUT);
  digitalWrite(pinHeure, HIGH);
  digitalWrite(pinSens, HIGH);

  pinMode(pinBouton, INPUT_PULLUP);
  pinMode(pinCapteur, INPUT);


  //********************************** configuration carte

  bool lSwitch = digitalRead (pinL_DipSwitch);
  bool hSwitch = digitalRead (pinH_DipSwitch);

  if (lSwitch & !hSwitch) {                    //  Carte Aiguille Heure
    Serial.println ("Aiguille Heure");
    c = 1;
  }

  else if ( ! lSwitch & hSwitch ) {
    Serial.println ("Aiguille Minutes");       //  Carte Aiguille Minutes
    s1 = 100; s2 = 150; s3 = 600;
    t1 = 36000, t2 = 11993, t3 = 120;
    c = 2;
  }

  else if ( !lSwitch & !hSwitch ) {
    Serial.println ("Aiguille Secondes");       //  Carte Aiguille Secondes
    s1 = 100; s2 = 150; s3 = 950;
    t1 = 4000; t2 = 190; t3 = 2;
    c = 3;
  }

  else if (lSwitch & hSwitch) {
    Serial.println ("Carte emetteur");
  }

  horlogeReset (1);

  Serial.println ("fin seput");

}

void loop() {

  //********************************** Reception

  //Serial.println(part2);
  if (serialReception.available()) {

    char c = serialReception.read(); //get the character

    if (c == '\n') {
      parseCommand(command);
      //Serial.println ( command);
      command = "";
    }
    else {
      command += c;
    }
  }

  //********************************** Anti Horaire


  if (part1.equalsIgnoreCase("a")) {

    int val = part2.toInt();

    if (val < s1) {
      int dureTemps = (map ( val, 0, s1, t1, t2));
      if (temps (dureTemps)) {
        anti_Horaire ();
      }
    }

    else if (s1 <= val && val <= s2)  { //temps seconde
      int dureTemps = (t2);
      if (temps (dureTemps)) {
        anti_Horaire ();
      }
    }

    else if ( s2 < val && val <= s3 ) {
      int dureTemps = (map ( val, s2, s3, t2, t3));
      if (temps (dureTemps)) {
        anti_Horaire ();
      }
    }

    else if ( s3 < val ) {
      int dureTemps = (map ( val, s3, 1022, t3, 2));
      switch (c) {
        case 2:
          if (temps (dureTemps)) {
            anti_Horaire ();
          }

          break;
        case 3:
          anti_Horaire ();
          break;
      }

    }
  }




  //*********************************** Stop

  else if (part1.equalsIgnoreCase("s")) {
    int val = part2.toInt();
    //Serial.println ("Stop");
  }

  //*********************************** Sens Horaire

  else if (part1.equalsIgnoreCase("h")) {
    int val = part2.toInt();
    //Serial.println(val);

    if (val < s1) {
      int dureTemps = (map ( val, 0, s1, t1, t2));
      if (temps (dureTemps)) {
        sens_Horaire ();
      }
    }

    else if (s1 <= val && val <= s2)  {
      int dureTemps = (t2);
      if (temps (dureTemps)) {
        sens_Horaire ();
      }
    }

    else if ( s2 < val && val <= s3 ) {
      int dureTemps = (map ( val, s2, s3, t2, t3));
      if (temps (dureTemps)) {
        sens_Horaire ();
      }
    }

    else if ( s3 < val ) {
      int dureTemps = (map ( val, s3, 1023, t3, 2));

      switch (c) {
        case 2:
          if (temps (dureTemps)) {
            sens_Horaire ();
          }

          break;
        case 3:
          sens_Horaire ();
          break;
      }

    }

  }

  //**************************************  Reset

  else if (part1.equalsIgnoreCase("r")) {
    int val = part2.toInt();
    horlogeReset (val);
  }

  //  ************************************   Bad commande

  else {
    //Serial.println("COMMAND NOT RECOGNIZED");
  }
}


